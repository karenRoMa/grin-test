# iOS Grin Test por Luis Chávez

Primero que nada me gustaría agredecer la oportunidad de haber realizado la prueba y el tiempo otorgado para la revisión de la misma. Agradecería recibir algún tipo de feedback sobre la prueba.

# ¿Cómo se resolvió reto?

  1. Antes de empezar a 'programar sin razón', me gusta analizar el problema y ver que herramientas o recursos puedo utilizar, es decir, que dependencias utilizaré, el patrón de diseño que mejor se adapta y la forma en la que estructuraría el proyecto.
  2. Por consecuente procedí a realizar la configuración de mi proyecto en xcode y el repositorio de git, agregando un archivo '.gitignore' con reglas que me permiten trabajar mejor.
  3. Una vez completado eso agregué las dependencias que utilizaría con Cocoapods para empezar a realizar mis modelos para consumir de los servicios web de prueba.
  4. Teniendo eso listo procedí a realizar las pruebas unitarias de los servicios web, para verificar que mis modelos y manejadores funcionaran bien. Cabe recalcar que en las pruebas unitarias, realice las mínimas necesarias, pues al ser servicios de prueba creo que el reto no son las pruebas unitarias, sino la integración de tus dependencias y el código de tu proyecto base.
  5. Listo lo anterior, comencé a trabajar en lo que seria el primer controlador. Empecé a trabajar las subclases de los elementos que utilizaría, como el botón de refrescar y el marcador del mapa.
  6. Continué con el desarrollo del controlador de mapa, el cual se encargaría únicamente de la interación de las vistas, el manejo de datos sería dado por el viewmodel. Consideró que el mayor reto de la vista del mapa es lograr un buen manejo de los marcadores y la interacción del usuario.
  -- Nota: En el view model, agregué una tipo de dato el cual permitirá cambiar la forma en la cual se muestra del detalle de los 'dispositivos' en el mapa.
  7. Se procedó a elaborar la vista del listado de dispositivos bluetooth. El manejo e datos y mostrarlo no tiene mayor dificultad, considero que la razón de dicho reto es el manejo de bluetooth, el óptimo uso de escaneo de dipositivos así como la experiencia de usuario. :)
  8. En el último punto, procedí a realizar subclases de elementos visuales como lo es el UITextView y UIView. Con el propósito de utilizar la herramienta que nos provee xcode de realizar un render dentro del Interface Builder para ahorrarnos tiempo. Los invito a editar los parámetros del texto para probar la adaptabilidad de la vista desde el inspector seleccionando las vistas.
  9. Por consiguiente procedí a unificar los 3 controladores como 'hijos' de un TabBarController. :)
  10. Documentar y order el código con SwiftLint. Esta práctica me gusta mucho porque te ayuda a revisar la forma en la que programas y mejorar el código.

### Dependencias

Para resolver la prueba técnica, utilicé Cocoapods, que además de ser mi administrador de dependencias favorito considero que es el que tiene un mayor soporte por la comunidad.

```ruby
  pod 'Alamofire'
  pod 'SwiftyBeaver'
  pod 'AlamofireObjectMapper'
  pod 'MaryPopin'
  pod 'JTProgressHUD'
  pod 'TPKeyboardAvoiding'
  pod 'SwiftLint'
```

* [SwiftyBeaver](https://github.com/SwiftyBeaver/SwiftyBeaver) - Es una herramienta para mejorar el sistema de logs en la consola de Xcode y la cual tambien ofrecé un servicio en la nube. Esta herramienta me gusta por el soporte que puede dar a herramientas de analyticos que permiten registrar de forma remota ciertos logs en los dispositivos de los usuarios y nos pueden ayudar a mejorar issues que en un proceso de desarrollo y QA no detectamos. Realicé un par de anotaciones dentro del proyecto.
* [AlamofireObjectMapper](https://github.com/tristanhimmelman/AlamofireObjectMapper) - Es una bibloteca que nos ayuda a la serialización de las respuestas JSON de los servicios web, considero esta la mejor puesto que tiene un excelente soporte con Alamofire y de la comunidad.
* [MaryPopin](https://github.com/Backelite/MaryPopin) - La biblioteca es una de mis favoritas desde hace tiempo, la propuse para el uso de una vista modal, puesto que brindá la posibilidad de utilizar subclases de UIViewController con animaciones y efectos como parallax.
* [JTProgressHUD](https://github.com/kubatruhlar/JTProgressHUD) - Así como MaryPoing, JTProgressHUD se ha convertido en una de mis dependencias favoritas al iniciar un proyecto, puesto que ofrecé una forma elegante de mostrar al usuario una animación mientras un proceso en segundo plano ocurré. Es muy fácil de usar y personalizar. 
* [TPKeyboardAvoiding](https://github.com/michaeltyson/TPKeyboardAvoiding) - Es una solución para la interacción de ScrollViews y campos de texto, una de mis favoritas y excenciales para el desarrollo de aplicaciones iOS.
* [SwiftLint](https://github.com/realm/SwiftLint) - Es una solución que propone la comunidad para usar las convenciones de programación en Swift.

### Comentarios

 - Agregué un script que les mostrará las etiquetas marcadas como se muestra a continuación. Con el objectivo de ilustar/explicar código del proyecto.
 
 ```swift
//------------------------------------
//  CHECK: - Description
//------------------------------------
```

**Free Software, Hell Yeah!**
