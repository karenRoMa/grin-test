//
//  MapViewController.swift
//  Grin Test
//
//  Created by Luis Chávez on 12/20/18.
//  Copyright © 2018 Luis Chávez. All rights reserved.
//

import UIKit
import MapKit

class MapViewController: UIViewController {
    
    //------------------------------------
    //  MARK: - IBOutlets
    //------------------------------------

    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var reloadButton: ReloadButton!
    
    //------------------------------------
    //  COMMENT: - Ahora se carca un containver view con un hijo del este controllador. :) No implica mayor esfuerzo.
    //------------------------------------
    
    @IBOutlet weak var deviceDetailContainerView: UIView!
    
    //------------------------------------
    //  MARK: - View Model
    //------------------------------------
    
    var viewModel: MapViewModel = MapViewModel()
    
    //------------------------------------
    //  MARK: - Controller Life Cycle Methods
    //------------------------------------
    
    fileprivate var miniDeviceDetailVC: MiniDeviceDetailViewController?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        mapView.delegate = self
        mapView.showsUserLocation = true
        mapView.showsPointsOfInterest = false
        mapView.isRotateEnabled = false
        mapView.isPitchEnabled = false
        mapView.showsBuildings = true
        mapView.mapType = .mutedStandard
        
        if viewModel.detailViewType == .viewController {            
            deviceDetailContainerView.removeFromSuperview()
        }
        
        switch viewModel.detailViewType {
        case .viewController:
            deviceDetailContainerView.removeFromSuperview()
        case .view:
            if miniDeviceDetailVC == nil {
                fatalError("Check storyboard for missing MiniDeviceDetailViewController")
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        fetchDevices()
    }
    
    //------------------------------------
    //  MARK: - IBActions
    //------------------------------------
    
    @IBAction func reload(_ sender: Any) {
        fetchDevices()
    }
    
    //------------------------------------
    //  MARK: - Prepare for segue :)
    //------------------------------------

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        super.prepare(for: segue, sender: sender)
        
        if let childViewController = segue.destination as? MiniDeviceDetailViewController {
            miniDeviceDetailVC = childViewController
            miniDeviceDetailVC?.view.translatesAutoresizingMaskIntoConstraints = false
        }
        
    }
    
}

//------------------------------------
//  MARK: - Private methods for managing the controller.
//------------------------------------

extension MapViewController {
    
    /// Fetch devices from WS.
    private func fetchDevices() {
        reloadButton.startLoading()
        viewModel.reload { [weak self] (_) in
            guard let self = self else { return }
            self.reloadButton.stopLoading()
            self.refreshView()
        }
    }
    
    //------------------------------------
    //  COMMENT: - Se comentó que había un pequeño gltch en el mapa, creo que era al momento de hacer reload. Lo corregí y hace el reload sobre las anotaciones existente, no las elimina y vuelve a crear.
    //------------------------------------

    
    /// Refresh map view annotations.
    private func refreshView() {
        
        if mapView.annotations.isEmpty {
            
            let annotations = viewModel.devices.map({ DeviceAnnotation(device: $0) })
            mapView.addAnnotations(annotations)
            mapView.zoomAllAnnotations()
            
        } else {
            
            guard let annotations = mapView.annotations as? [DeviceAnnotation] else { return }
            
            for annotation in annotations {
                if let device = viewModel.devices.first(where: { annotation.device == $0 }) {
                    annotation.update(device: device)
                } else {
                    mapView.removeAnnotation(annotation)
                }
            }
            
            mapView.selectedAnnotations.forEach { (annotation) in
                self.mapView.deselectAnnotation(annotation, animated: true)
            }
            
            mapView.zoomAllAnnotations()

        }
        
    }
    
    //------------------------------------
    //  CHECK: - You can change the detail view, you can show as view or viewcontroller. ;)
    //------------------------------------
    
    /// Show detail view, the detail view can be a view or viewcontroller.
    ///
    /// - Parameter view: MKAnnotationView selected.
    private func selectAnnotation(view: MKAnnotationView) {
        
        guard let annotation = view.annotation as? DeviceAnnotation else { return }
        
        UIImpactFeedbackGenerator(style: .light).impactOccurred()

        switch viewModel.detailViewType {
        case .view:
            showDetailView(with: annotation)
        case .viewController:
            showDetailViewController(with: annotation)
        }
        
    }
        
    /// Detail view for device for show device information.
    ///
    /// - Parameter annotation: DeviceAnnotation selected.
    private func showDetailView(with annotation: DeviceAnnotation) {
        
        miniDeviceDetailVC?.config(device: annotation.device)
        deviceDetailContainerView.fadeIn(withDuration: 0.4)
        let topPadding = deviceDetailContainerView.bounds.origin.y + deviceDetailContainerView.bounds.height + 64
        let padding = UIEdgeInsets(top: topPadding, left: 64, bottom: 64, right: 64)
        mapView.zoom(annotation: annotation, padding: padding)
        
    }
    
    /// Detail view controller for show device information.
    ///
    /// - Parameter annotation: DeviceAnnotation selected.
    private func showDetailViewController(with annotation: DeviceAnnotation) {
        let deviceDetailVC = DeviceDetailViewController.popinViewController(device: annotation.device) {
            self.mapView.deselectAnnotation(annotation, animated: true)
        }
        tabBarController?.presentPopinController(deviceDetailVC, animated: true, completion: nil)
        mapView.zoom(annotation: annotation)
    }
    
}

//------------------------------------
//  MARK: - MKMapViewDelegate
//------------------------------------

extension MapViewController: MKMapViewDelegate {
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
                
        if annotation.isEqual(mapView.userLocation) {
            let userLocation: MKUserLocation = mapView.userLocation
            userLocation.title = "My location"
            return nil
        }
        
        if annotation is DeviceAnnotation {

            let annotationIdentifier = "AnnotationIdentifier"
            
            var annotationView: MKAnnotationView?
            if let dequeuedAnnotationView = mapView.dequeueReusableAnnotationView(withIdentifier: annotationIdentifier) {
                annotationView = dequeuedAnnotationView
                annotationView?.annotation = annotation
            } else {
                annotationView = DeviceAnnotationView(annotation: annotation,
                                                      reuseIdentifier: annotationIdentifier)
            }
            
            annotationView?.canShowCallout = false
            
            return annotationView

        }
        
        return nil
    }
    
    func mapView(_ mapView: MKMapView, didSelect view: MKAnnotationView) {
        selectAnnotation(view: view)
    }
    
    func mapView(_ mapView: MKMapView, didDeselect view: MKAnnotationView) {
        if viewModel.detailViewType == .view {
            deviceDetailContainerView.fadeOut(withDuration: 0.1)
        }
    }
    
    func mapView(_ mapView: MKMapView, didAdd views: [MKAnnotationView]) {
        views.forEach { $0.alpha = 0 }
        UIView.animate(withDuration: 0.35,
                       delay: 0,
                       usingSpringWithDamping: 1,
                       initialSpringVelocity: 0,
                       options: [], animations: {
                        views.forEach { $0.alpha = 1 }
        }, completion: nil)
    }
    
}

//------------------------------------
//  MARK: - Extension for MKMapView.
//------------------------------------

extension MKMapView {
    
    /// Zoom to all annotations added in mapview.
    ///
    /// - Parameter padding: Inset for show all annotations inside mapview.
    func zoomAllAnnotations(padding: UIEdgeInsets = UIEdgeInsets(top: 64,
                                                                 left: 64,
                                                                 bottom: 64,
                                                                 right: 64)) {
        
        var zoomRect = MKMapRect.null
        for annotation in annotations {
            let annotationPoint = MKMapPoint(annotation.coordinate)
            let pointRect = MKMapRect(x: annotationPoint.x,
                                      y: annotationPoint.y,
                                      width: 0,
                                      height: 0)
            if zoomRect.isNull {
                zoomRect = pointRect
            } else {
                zoomRect = zoomRect.union(pointRect)
            }
        }
        setVisibleMapRect(zoomRect,
                          edgePadding: padding,
                          animated: true)
        
    }
    
    /// Zoom to specific annotation.
    ///
    /// - Parameters:
    ///   - annotation: Annotation to zoom in.
    /// - Parameter padding: Inset for show all annotations inside mapview.
    func zoom(annotation: MKAnnotation,
              padding: UIEdgeInsets = UIEdgeInsets(top: 64,
                                                   left: 64,
                                                   bottom: 64,
                                                   right: 64)) {
        
        let region = MKCoordinateRegion(center: annotation.coordinate,
                                        latitudinalMeters: 200,
                                        longitudinalMeters: 200)
        
        setVisibleMapRect(MKMapRectForCoordinateRegion(region: region),
                          edgePadding: padding,
                          animated: true)

    }
    
    /// Fuction to get a MKMapRect form a MKCoordinateRegion. https://stackoverflow.com/a/35321619
    ///
    /// - Parameter region: MKCoordinateRegion to convert.
    /// - Returns: Region as a MKMapRect instance.
    private func MKMapRectForCoordinateRegion(region: MKCoordinateRegion) -> MKMapRect {
        let topLeft = CLLocationCoordinate2D(latitude: region.center.latitude + (region.span.latitudeDelta/2),
                                             longitude: region.center.longitude - (region.span.longitudeDelta/2))
        let bottomRight = CLLocationCoordinate2D(latitude: region.center.latitude - (region.span.latitudeDelta/2),
                                                 longitude: region.center.longitude + (region.span.longitudeDelta/2))
        
        let topLeftPoint = MKMapPoint(topLeft)
        let bottomRightPoint = MKMapPoint(bottomRight)
        
        return MKMapRect(origin: MKMapPoint(x: min(topLeftPoint.x, bottomRightPoint.x),
                                            y: min(topLeftPoint.y, bottomRightPoint.y)),
                         size: MKMapSize(width: abs(topLeftPoint.x-bottomRightPoint.x),
                                         height: abs(topLeftPoint.y-bottomRightPoint.y)))
    }
    
}
