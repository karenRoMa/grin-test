//
//  GrinButton.swift
//  Grin Test
//
//  Created by Luis Chávez on 12/21/18.
//  Copyright © 2018 Luis Chávez. All rights reserved.
//

import UIKit

@IBDesignable
class GrinButton: UIButton {
    
    //------------------------------------
    //  MARK: - Properties
    //------------------------------------

    /// Corner radius for button.
    @IBInspectable var cornerRadius: CGFloat = 10.0 {
        didSet {
            updateLayer()
        }
    }
    
    /// Border width for text view.
    @IBInspectable var borderWidth: CGFloat = 2.0 {
        didSet {
            updateLayer()
        }
    }
    
    /// Border color for text view.
    @IBInspectable var borderColor: UIColor = .darkGray {
        didSet {
            updateLayer()
        }
    }
    
    //------------------------------------
    //  MARK: - Overloaded methods
    //------------------------------------
    
    override func layoutSubviews() {
        super.layoutSubviews()
        updateLayer()
    }
    
    override func prepareForInterfaceBuilder() {
        super.prepareForInterfaceBuilder()
        
    }
    
    //------------------------------------
    //  MARK: - Private methods
    //------------------------------------
    
    private func updateLayer() {
        layer.cornerRadius = cornerRadius
        layer.borderWidth = borderWidth
        layer.borderColor = borderColor.cgColor
        layer.masksToBounds = true
    }

}
