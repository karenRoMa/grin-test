//
//  PlaceHolderTextView.swift
//  Grin Test
//
//  Created by Luis Chávez on 12/21/18.
//  Copyright © 2018 Luis Chávez. All rights reserved.
//

import UIKit

class PlaceHolderTextView: UITextView {

    //------------------------------------
    //  MARK: - Properties
    //------------------------------------

    // Placeholder text color.
    @IBInspectable var placeholderColor: UIColor = .lightGray {
        didSet {
            textColor = placeholderColor
        }
    }
    
    // Placeholder text.
    @IBInspectable var placeholder: String = "Hi!" {
        didSet {
            text = placeholder
        }
    }

    /// Corner radius for text view.
    @IBInspectable var cornerRadius: CGFloat = 10.0 {
        didSet {
            updateLayer()
        }
    }
    
    /// Border width for text view.
    @IBInspectable var borderWidth: CGFloat = 2.0 {
        didSet {
            updateLayer()
        }
    }
    
    /// Border color for text view.
    @IBInspectable var borderColor: UIColor = .darkGray {
        didSet {
            updateLayer()
        }
    }
    
    //------------------------------------
    //  MARK: - Initializers
    //------------------------------------
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    //------------------------------------
    //  MARK: - Overloaded methods
    //------------------------------------

    override func prepareForInterfaceBuilder() {
        super.prepareForInterfaceBuilder()
        
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        updateLayer()
    }
    
    //------------------------------------
    //  MARK: - Private methods
    //------------------------------------
    
    /// Update the layer from layer properties.
    private func setup() {
        delegate = self
        textContainerInset = UIEdgeInsets(top: 12,
                                          left: 12,
                                          bottom: 12,
                                          right: 12)
    }
    
    /// Setup view from XIB file.
    private func updateLayer() {
        layer.cornerRadius = cornerRadius
        layer.borderWidth = borderWidth
        layer.borderColor = borderColor.cgColor
        layer.masksToBounds = true
    }

}

//------------------------------------
//  MARK: - UITextViewDelegate
//------------------------------------

extension PlaceHolderTextView: UITextViewDelegate {
    
    //------------------------------------
    //  COMMENT: - Uso indiscriminado de pods sería el usar un pod para mostrar un placeholder en un UITextView, el cual pude resolver rapidamente usando UITextViewDelegate y simples sentencias if.
    //------------------------------------

    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == placeholderColor {
            textView.text = nil
            textView.textColor = UIColor.black
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = placeholder
            textView.textColor = placeholderColor
        }
    }
    
}
