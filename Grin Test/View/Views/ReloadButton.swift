//
//  ReloadButton.swift
//  Grin Test
//
//  Created by Luis Chávez on 12/20/18.
//  Copyright © 2018 Luis Chávez. All rights reserved.
//

import UIKit

@IBDesignable
class ReloadButton: UIButton {
    
    /// Change color of UIActivityIndicatorView and tint color.
    @IBInspectable var indicatorColor: UIColor = .white {
        didSet {
            tintColor = indicatorColor
        }
    }
    
    /// Image that will be the icon when button is not loading.
    @IBInspectable var icon: UIImage = UIImage() {
        didSet {
            setImage(icon, for: .normal)
        }
    }
    
    var isLoading: Bool {
        return activityIndicator.isAnimating
    }
    
    //------------------------------------
    //  MARK: - Private properties
    //------------------------------------

    private lazy var activityIndicator: UIActivityIndicatorView = {
        let activityIndicator = UIActivityIndicatorView()
        activityIndicator.hidesWhenStopped = true
        activityIndicator.color = indicatorColor
        activityIndicator.isUserInteractionEnabled = false
        return activityIndicator
    }()

    //------------------------------------
    //  MARK: - Overloaded methods
    //------------------------------------

    override func layoutSubviews() {
        super.layoutSubviews()
        
        layer.shadowColor = UIColor.black.cgColor
        layer.shadowOffset = CGSize(width: 0, height: 0)
        layer.shadowRadius = 3
        layer.shadowOpacity = 0.5
        layer.cornerRadius = frame.size.width/2.0
        layer.masksToBounds = false
        
    }
    
    override func prepareForInterfaceBuilder() {
        super.prepareForInterfaceBuilder()
        
    }

    //------------------------------------
    //  MARK: - Public methods
    //------------------------------------

    /// Starts the animation of the progress indicator.
    func startLoading() {
        isUserInteractionEnabled = false
        setImage(nil, for: .normal)
        showIndicatorView()
    }
    
    /// Stops the animation of the progress indicator.
    func stopLoading() {
        isUserInteractionEnabled = true
        setImage(icon, for: .normal)
        activityIndicator.stopAnimating()
    }
    
    //------------------------------------
    //  MARK: - Private methods
    //------------------------------------
    
    private func showIndicatorView() {
        activityIndicator.translatesAutoresizingMaskIntoConstraints = false
        addSubview(activityIndicator)
        centerActivityIndicatorView()
        activityIndicator.startAnimating()
    }
    
    private func centerActivityIndicatorView() {
        
        let xCenterConstraint = NSLayoutConstraint(item: self,
                                                   attribute: .centerX,
                                                   relatedBy: .equal,
                                                   toItem: activityIndicator,
                                                   attribute: .centerX,
                                                   multiplier: 1,
                                                   constant: 0)
        
        let yCenterConstraint = NSLayoutConstraint(item: self,
                                                   attribute: .centerY,
                                                   relatedBy: .equal,
                                                   toItem: activityIndicator,
                                                   attribute: .centerY,
                                                   multiplier: 1,
                                                   constant: 0)
        
        addConstraint(xCenterConstraint)
        addConstraint(yCenterConstraint)
        
    }

}
