//
//  BluetoothScannerViewModel.swift
//  Grin Test
//
//  Created by Luis Chávez on 12/21/18.
//  Copyright © 2018 Luis Chávez. All rights reserved.
//

import Foundation
import CoreBluetooth

class BluetoothScannerViewModel: NSObject {
    
    //------------------------------------
    //  MARK: - Public properties
    //------------------------------------
    
    /// Block execute when a device is found and the view should be reloaded.
    var shouldReloadView: (() -> Void)?
    
    /// Block execute when the manager stop scanning.
    var didStopScanBlock: (() -> Void)?
    
    /// Block executes when the Bluetooth is unavailable or turned off.
    var bluetoothDisabledBlock: (() -> Void)?

    //------------------------------------
    //  COMMENT: - Dentro del feedback comentaron el uso de variables lazy en esta clase, esta es una variable computada, lo cual crea una interfaz de la variable 'isScanning' del manejador del bluetooth, con el fin de mantener la encapsulación del mismo como propiedad privada.
    //------------------------------------

    /// Whether or not the central is currently scanning
    var isScanning: Bool {
        return manager.isScanning
    }
    
    //------------------------------------
    //  MARK: - Private properties
    //------------------------------------

    private (set) var devices: [Device] = []
    private var manager: CBCentralManager
    
    //------------------------------------
    //  COMMENT: - Si por el uso de variable lazy era la inicialización del manejador del bluetooth, se podría cambiar al sobrecargar el inicializador de la clase.
    //------------------------------------

    override init() {
        manager = CBCentralManager(delegate: nil,
                                   queue: DispatchQueue.global(qos: .background))
        super.init()
    }
    
    //------------------------------------
    //  MARK: - Public methods
    //------------------------------------

    /// Scans for peripherals, scans stop after 2.5 seconds.
    func scan() {

        manager.delegate = self
        
        devices.removeAll()
        shouldReloadView?()
        
        switch manager.state {
        case .poweredOff, .resetting, .unsupported, .unauthorized, .unknown:
            bluetoothDisabledBlock?()
            return
        case .poweredOn: break
        }

        manager.scanForPeripherals(withServices: nil, options: nil)
        
        //------------------------------------
        //  COMMENT: - También se comentó el '¿Por qué?' detenia el scan con un timer. Bien podría añadir que al encontrar el primer dispositivo bluetooth detuviera la búsqueda. Lo que yo haría como dev de Grin (Y estoy casi seguro que lo hacen asi), es que desde un ws reciben los identificadores del bluetooth del patín, y proceden a buscarlo mediante una lista usando 'scanForPeripherals(withServices: nil, options: nil)', una vez encontrado detienen la búsqueda y proceden a conectarse. Best Practices for Interacting with a Remote Peripheral Device -> https://developer.apple.com/library/archive/documentation/NetworkingInternetWeb/Conceptual/CoreBluetooth_concepts/BestPracticesForInteractingWithARemotePeripheralDevice/BestPracticesForInteractingWithARemotePeripheralDevice.html
        //------------------------------------
        DispatchQueue.main.asyncAfter(deadline: .now() + 2.5) {
            self.manager.stopScan()
            self.didStopScanBlock?()
        }
        
    }
    
    /// Save device in WS.
    ///
    /// - Parameters:
    ///   - device: Device information object.
    ///   - completion: A closure to be executed once the request has finished.
    func save(device: Device, _ completion: @escaping GrinCompletion) {
        
        let parameters: [String: Any] = ["name": String(format: "{%@}", device.name),
                                         "strength": String(format: "{%@}", device.strength)]
        
        APIManager.shared.createBluetoothDevice(parameters: parameters) { (_, error) in
            completion(error)
        }
        
    }
    
}

//------------------------------------
//  MARK: - CBCentralManagerDelegate
//------------------------------------

extension BluetoothScannerViewModel: CBCentralManagerDelegate {
    
    func centralManagerDidUpdateState(_ central: CBCentralManager) {
        
        DispatchQueue.main.sync {
            switch central.state {
            case .poweredOff, .resetting, .unsupported, .unauthorized, .unknown:
                self.bluetoothDisabledBlock?()
            case .poweredOn:
                self.scan()
            }
        }

    }
    
    func centralManager(_ central: CBCentralManager, didDiscover peripheral: CBPeripheral, advertisementData: [String: Any], rssi RSSI: NSNumber) {
        
        if devices.contains(where: { $0.id == peripheral.identifier.uuidString }) == false {
            DispatchQueue.main.sync {
                self.devices.append(Device(peripheral: peripheral, rssi: RSSI.stringValue))
                self.shouldReloadView?()
            }
        }
        
    }

}
