//
//  MapViewModel.swift
//  Grin Test
//
//  Created by Luis Chávez on 12/20/18.
//  Copyright © 2018 Luis Chávez. All rights reserved.
//

import Foundation

enum DetailType {
    case view
    case viewController
}

class MapViewModel {
    
    //------------------------------------
    //  MARK: - Limited access properties
    //------------------------------------
    
    /// Property to chose the detail view type.
    private (set) var detailViewType: DetailType = .view
    
    /// Array of devices fetched from WS.
    private (set) var devices: [Device] = []
    
    //------------------------------------
    //  MARK: - Public methods
    //------------------------------------
    
    /// Fetch the devices from WS.
    ///
    /// - Parameter completion: A closure to be executed once the request has finished.
    func reload(_ completion: @escaping GrinCompletion) {
        
        APIManager.shared.getAllBluetoothDevices(parameters: ["order": 1]) { (devices, error) in
            self.devices = devices
            completion(error)
        }
        
    }
    
}
