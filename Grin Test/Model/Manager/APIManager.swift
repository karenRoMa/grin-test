//
//  APIManager.swift
//  Grin Test
//
//  Created by Luis Chávez on 12/20/18.
//  Copyright © 2018 Luis Chávez. All rights reserved.
//

import Foundation
import Alamofire
import AlamofireObjectMapper
import ObjectMapper

//------------------------------------
//  MARK: - Definition of response blocks
//------------------------------------

typealias DevicesCompletion = (_ devices: [Device], _ error: GrinError?) -> Void
typealias DeviceCompletion = (_ device: Device?, _ error: GrinError?) -> Void

class APIManager {
    static let shared = APIManager()
}

extension APIManager {
    
    private func retrieveResult<T>(_ response: DataResponse<APIResponse<T>>,
                                   withError description: String) -> (result: T?, error: GrinError?) {
        
        switch response.result {
        case .success(let value):
            
            switch value.status {
            case .success:
                if let object = value.object {
                    return (object, nil)
                } else {
                    return(nil, GrinError(description: description,
                                          additionalError: nil))
                }
            case .fail:
                return(nil, GrinError(description: description,
                                      additionalError: value.message))
            }
            
        case .failure(let error):
            return(nil, GrinError(description: description,
                                  additionalError: error.localizedDescription))
        }
        
    }
    
    private func retrieveResults<T>(_ response: DataResponse<APIResponse<T>>,
                                    withError description: String) -> (results: [T], error: GrinError?) {
        
        switch response.result {
        case .success(let value):
            
            switch value.status {
            case .success:
                return (value.objects, nil)
            case .fail:
                return([], GrinError(description: description,
                                     additionalError: value.message))
            }
            
        case .failure(let error):
            return([], GrinError(description: description,
                                 additionalError: error.localizedDescription))
        }
        
    }
    
}

extension APIManager {
    
    /// Method to retrieve all bluetooth devices.
    ///
    /// - Parameters:
    ///   - parameters: Parameters is a [String: Any] typealias.
    ///   - completion: A closure to be executed once the request has finished.
    func getAllBluetoothDevices(parameters: Parameters? = nil, completion: @escaping DevicesCompletion) {
        
        API.bluetoothAll
            .request(parameters: parameters)
            .responseObject { (response: DataResponse<APIResponse<Device>>) in
                let result = self.retrieveResults(response, withError: "An error ocurred with the server!")
                completion(result.results, result.error)
            }
        
    }
    
    /// Method to save a bluetooth device.
    ///
    /// - Parameters:
    ///   - parameters: Parameters is a [String: Any] typealias.
    ///   - completion: A closure to be executed once the request has finished.
    func createBluetoothDevice(parameters: Parameters, completion: @escaping DeviceCompletion) {
                
        API.bluetoothCreate
            .request(parameters: parameters)
            .responseObject { (response: DataResponse<APIResponse<Device>>) in
                let result = self.retrieveResult(response, withError: "An error ocurred with the server!")
                completion(result.result, result.error)
            }
        
    }
    
}
