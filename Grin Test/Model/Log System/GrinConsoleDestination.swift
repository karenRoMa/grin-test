//
//  GrinConsoleDestination.swift
//  Grin Test
//
//  Created by Luis Chávez on 12/20/18.
//  Copyright © 2018 Luis Chávez. All rights reserved.
//

import Foundation
import SwiftyBeaver

/// GrinConsoleDestination is an helper to read with more readability all our logs in console. This class can be used for store all logs with Analytics systems.
final class GrinConsoleDestination: BaseDestination {
    
    //------------------------------------
    //  MARK: - Initializers
    //------------------------------------

    override init() {
        
        super.init()
        
        self.levelString.verbose = "😇 VERBOSE"
        self.levelString.debug = "🧐 DEBUG"
        self.levelString.info = "🤔 INFO"
        self.levelString.warning = "😨 WARNING"
        self.levelString.error = "🤬 ERROR"
        
        self.format = ">>> $DHH:mm:ss $L:\n***\n\n$M\n\n***\n\n"
        
    }
    
    //------------------------------------
    //  MARK: - Overloaded methods
    //------------------------------------

    override func send(_ level: SwiftyBeaver.Level,
                       msg: String,
                       thread: String,
                       file: String,
                       function: String,
                       line: Int,
                       context: Any?) -> String? {
        
        let formattedString = super.send(level,
                                         msg: msg,
                                         thread: thread,
                                         file: file,
                                         function: function,
                                         line: line,
                                         context: context)
        
//------------------------------------
//  CHECK: - For example, we can use this method to store a log with Instabug log system. https://docs.instabug.com/docs/ios-logging#section-console-logs
//------------------------------------
        
//        if let message = formattedString {
//            switch level {
//            case .verbose:
//                IBGLog.logVerbose(message)
//            case .debug:
//                IBGLog.logDebug(message)
//            case .error:
//                IBGLog.logError(message)
//            case .warning:
//                IBGLog.logWarn(message)
//            case .info:
//                IBGLog.logInfo(message)
//            }
//        }
        
        if let formattedString = formattedString {
            print(formattedString)
        }
        
        return formattedString
        
    }
    
}
