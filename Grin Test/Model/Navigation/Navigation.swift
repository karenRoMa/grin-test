//
//  Navigation.swift
//  Grin Test
//
//  Created by Luis Chávez on 12/20/18.
//  Copyright © 2018 Luis Chávez. All rights reserved.
//

import Foundation
import UIKit

/// Private enum to define each storyboard file name.
///
/// - map: Map Storyboard.
/// - bluetooth: Bluetooth Storyboard.
/// - profile: Profile Storyboard.
private enum Storyboard: String {
    case map = "Map"
    case bluetooth = "Bluetooth"
    case profile = "Profile"
}

//------------------------------------
//  CHECK: - Why Storyboards? why? I love use storyboard, they are a great tool to create visual elements quickly, but in a big project we need use more that one storyboard to have no problem with version control and for seccionate the application flow for work better in a team.
//------------------------------------

fileprivate extension UIViewController {
    
    /// UIViewController reuse identifier as class name.
    static var reuseIdentifier: String { return String(describing: self) }
    
}

fileprivate extension UIStoryboard {
    
    /// Class function that create an instance of view controller from storyboard.
    ///
    /// - Parameter storyboard: Stortboard file.
    /// - Returns: A generic UIViewController instance.
    static func load<T: UIViewController>(from storyboard: Storyboard) -> T {
        let storyboard = UIStoryboard(name: storyboard.rawValue, bundle: nil)
        if let viewController = storyboard.instantiateViewController(withIdentifier: T.reuseIdentifier) as? T {
            return viewController
        }
        fatalError("UIViewController with identifier \(T.reuseIdentifier) not found. :(")
    }
    
}

//------------------------------------
//  MARK: - Extension where we can get our viewcontrollers with a simple line code. ;) So easy!
//------------------------------------

extension UIStoryboard {
    
    class var mapViewController: MapViewController {
        return load(from: .map) as MapViewController
    }
    
    class var bluetoothScannerViewController: BluetoothScannerViewController {
        return load(from: .bluetooth) as BluetoothScannerViewController
    }
    
    class var profileViewController: ProfileViewController {
        return load(from: .profile) as ProfileViewController
    }
    
    class var rootTabBarController: UITabBarController {
        
        let mapVC = UIStoryboard.mapViewController
        mapVC.tabBarItem = UITabBarItem(title: "Map",
                                        image: UIImage(named: "baseline_map_white_24pt"),
                                        tag: 0)
        
        let bluetoothScannerVC = UIStoryboard.bluetoothScannerViewController
        bluetoothScannerVC.tabBarItem = UITabBarItem(title: "Bluetooth",
                                                     image: UIImage(named: "baseline_bluetooth_white_24pt"),
                                                     tag: 1)

        let profileVC = UIStoryboard.profileViewController
        profileVC.tabBarItem = UITabBarItem(title: "Profile",
                                            image: UIImage(named: "baseline_supervised_user_circle_white_24pt"),
                                            tag: 2)
        
        let tabBarController = UITabBarController()
        
        tabBarController.viewControllers = [mapVC, bluetoothScannerVC, profileVC]
        tabBarController.tabBar.tintColor = UIColor.white
        tabBarController.tabBar.barTintColor = UIColor(red: 32/255, green: 32/255, blue: 54/255, alpha: 1.0)
        tabBarController.tabBar.isTranslucent = false
        
        return tabBarController
        
    }
    
    /// A simple class method that you can user for change the rootviewcontroller.
    ///
    /// - Parameter controller: UIViewController that will be replace the current root view controller.
    class func changeRootViewController(to controller: UIViewController) {
        
        guard let window = UIApplication.shared.keyWindow,
            let rootViewController = window.rootViewController else { return }
        
        controller.view.frame = rootViewController.view.frame
        controller.view.layoutIfNeeded()
        
        UIView.transition(with: window, duration: 0.3, options: .transitionFlipFromLeft, animations: {
            window.rootViewController = controller
        }, completion: { _ in })
        
    }
    
}
