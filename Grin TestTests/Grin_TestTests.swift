//
//  Grin_TestTests.swift
//  Grin TestTests
//
//  Created by Luis Chávez on 12/20/18.
//  Copyright © 2018 Luis Chávez. All rights reserved.
//

import XCTest
import Alamofire
import SwiftyBeaver

//------------------------------------
//  TODO: - Add unit test to fastlane as a lane before create a build. :)
//------------------------------------

let log = SwiftyBeaver.self

class Grin_TestTests: XCTestCase {

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
        log.addDestination(GrinConsoleDestination())
    }
    
    func testLog() {
        log.debug("Hi Grin!")
    }

    func testBluetoothAllWithoutParams() {
        
        let promise = expectation(description: "Request")
        
        APIManager.shared
            .getAllBluetoothDevices { (_, error) in
                XCTAssert(error != nil, "The error message must not be nil.")
                promise.fulfill()
        }
        
        waitForExpectations(timeout: 5)
        
    }
    
    func testBluetoothAll() {
        
        let promise = expectation(description: "Request")
        
        APIManager.shared
            .getAllBluetoothDevices(parameters: ["order": 1]) { (devices, error) in
                XCTAssertNil(error, "The error must be nil.")
                XCTAssertTrue(devices.count == 3, "Devices count must be equal to 3. :(")
                promise.fulfill()
        }
        
        waitForExpectations(timeout: 5)
        
    }
    
    func testSaveBluetoothDevice() {
        
        let promise = expectation(description: "Request")
        
        let parameters: Parameters = ["name": "{name}",
                                      "strength": "{-23db}"]
        
        APIManager.shared
            .createBluetoothDevice(parameters: parameters) { (device, error) in
                XCTAssertNil(error, "The error must be nil.")
                XCTAssertTrue(device != nil, "The device must not be nil.")
                promise.fulfill()
        }
        
        waitForExpectations(timeout: 5)
        
    }

}
